package com.pentatech.diyhealthcheckup.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pentatech.diyhealthcheckup.R;

/**
 * Created by Pasindu on 10/9/2017.
 */

public class Fragment_Patient_Start extends Fragment {

    private static final String G_TAG = "Global"; //Debugging

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        Log.d(G_TAG, "Patient segment created");
        return inflater.inflate(R.layout.fragment_patient_start, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("DIYHealthCheckup");

    }
}
