package com.pentatech.diyhealthcheckup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import com.pentatech.diyhealthcheckup.Fragments.Fragment_Doctor_Contactlist;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Doctor_Start;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Laboratory_Contactlist;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Laboratory_Start;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Manage_Reports_Laboratory;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_My_Profile_Laboratory;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Nav_Settings_Doctor;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Nav_Settings_Laboratory;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Notification_Laboratory;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Patient_Doctorlist;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_GetAppointment;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Laboratories;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Manage_Reports_Patient;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_My_Profile_Doctor;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_My_Profile_Patient;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Nav_Settings_Patients;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Notification_Doctor;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Notification_Patients;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Patient_Start;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Presc_Test_Details;
import com.pentatech.diyhealthcheckup.Fragments.Fragment_Time_Schedule;

public class Activity_Navigation_Bar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String current_nav_drawer; //Flags current Navigation drawer

    private static final String G_TAG = "Global"; //Debugging

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String rolename = getIntent().getExtras().getString("role"); //collecting the passed intent data

        //If passed intent data ==  Patient
        if (rolename.equals("Patient")) {

            setContentView(R.layout.activity_navigation__bar_patient);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            //First Fragment oncreate of this activity
            Fragment fragment = new Fragment_Patient_Start();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_patient);
            navigationView.setNavigationItemSelectedListener(this);


        }
        //If passed intent data ==  Doctor
        else if (rolename.equals("Doctor")) {
            setContentView(R.layout.activity_navigation__bar_doctor);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            //First Fragment oncreate of this activity
            Fragment fragment = new Fragment_Doctor_Start();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_doctor);
            navigationView.setNavigationItemSelectedListener(this);
        }
        //If passed intent data ==  Laboratory
        else {
            setContentView(R.layout.activity_navigation__bar_laboratory);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            //First Fragment oncreate of this activity
            Fragment fragment = new Fragment_Laboratory_Start();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_laboratory);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_laboratory);
            navigationView.setNavigationItemSelectedListener(this);
        }

    }

    @Override
    public void onBackPressed() {
        //Close drawer on BackPressed
        DrawerLayout drawer_patient = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
        DrawerLayout drawer_doctor = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
        DrawerLayout drawer_laboratory = (DrawerLayout) findViewById(R.id.drawer_layout_laboratory);
        if (drawer_patient.isDrawerOpen(GravityCompat.START)) {
            drawer_patient.closeDrawer(GravityCompat.START);
        } else if (drawer_doctor.isDrawerOpen(GravityCompat.START)) {
            drawer_doctor.closeDrawer(GravityCompat.START);
        } else if (drawer_laboratory.isDrawerOpen(GravityCompat.START)){
            drawer_laboratory.closeDrawer(GravityCompat.START);
        } else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation__bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;

        int id = item.getItemId();

        //For the drawer items in patient
        if (id == R.id.nav_profile_patient) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_My_Profile_Patient();
        } else if (id == R.id.nav_notifications_patients) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Notification_Patients();
        } else if (id == R.id.nav_doctors) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Patient_Doctorlist();
        } else if (id == R.id.nav_laboratories) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Laboratories();
        } else if (id == R.id.nav_get_an_appointment) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_GetAppointment();
        } else if (id == R.id.nav_manage_reports) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Manage_Reports_Patient();
        } else if (id == R.id.nav_prescription_tests) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Presc_Test_Details();
        } else if (id == R.id.nav_settings) {
            current_nav_drawer = "Patient";
            fragment = new Fragment_Nav_Settings_Patients();
        }
        //For the drawer items in Doctor
        else if (id == R.id.nav_profile_doc){
            current_nav_drawer = "Doctor";
            fragment = new Fragment_My_Profile_Doctor();
        } else if (id == R.id.nav_notifications_doc){
            current_nav_drawer = "Doctor";
            fragment = new Fragment_Notification_Doctor();
        } else if (id == R.id.nav_time_schedules){
            current_nav_drawer = "Doctor";
            fragment = new Fragment_Time_Schedule();
        } else if (id == R.id.nav_docs_contacts){
            current_nav_drawer = "Doctor";
            fragment = new Fragment_Doctor_Contactlist();
        } else if (id == R.id.nav_settings_doc){
            current_nav_drawer = "Doctor";
            fragment = new Fragment_Nav_Settings_Doctor();
        }
        // for the drawer item in Laboratory
        else if (id == R.id.nav_profile_laboratory){
            current_nav_drawer = "Laboratory";
            fragment = new Fragment_My_Profile_Laboratory();
        } else if (id == R.id.nav_notifications_laboratory){
            current_nav_drawer = "Laboratory";
            fragment = new Fragment_Notification_Laboratory();
        } else if (id == R.id.nav_manage_reports_laboratory){
            current_nav_drawer = "Laboratory";
            fragment = new Fragment_Manage_Reports_Laboratory();
        } else if (id == R.id.nav_laboratory_contacts){
            current_nav_drawer = "Laboratory";
            fragment = new Fragment_Laboratory_Contactlist();
        } else if (id == R.id.nav_settings_laboratory){
            current_nav_drawer = "Laboratory";
            fragment = new Fragment_Nav_Settings_Laboratory();
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        //Should close the relevant navigation drawer once the fragment is inflated
        if (current_nav_drawer.equals("Patient")) {
            //If drawer is Patients
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        } else if (current_nav_drawer.equals("Doctor")) {
            //If drawer is Doctors
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        } else {
            //If drawer is Lab
            DrawerLayout drawer_laboratory = (DrawerLayout) findViewById(R.id.drawer_layout_laboratory);
            drawer_laboratory.closeDrawer(GravityCompat.START);
            return true;
        }

    }
}
